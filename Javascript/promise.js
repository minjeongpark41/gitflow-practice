const promise = new Promise(function(resolve, reject){
    setTimeout(function(){
        const name = "park";
        console.log(name);
        //resolve(name);
        reject(name);
    }, 0)
})

promise.then(function(name){
    console.log(name)
})
.catch(function (err){
    //console.log(name); -> 에러남
    console.log(err); //-> park 나옴. reject(실패 인자)가 여기로 오는 것
})