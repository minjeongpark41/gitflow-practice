const User = function(name){
    this.name = name;
}

async function findPark(){
    const a = new User('park');
    let user = await a.name;
    console.log(typeof user, user);
}

async function findPark2(){
    const a = new User('park');
    let user = a.name;
    console.log(typeof user, user);
}

findPark();
findPark2();
