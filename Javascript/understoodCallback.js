var a = [];


function print123 (userdata){
    if (typeof userdata === "string"){
        console.log(userdata)
    } else if (typeof  userdata === "object"){
        for (var item in userdata){
            console.log(item + ";;;" + userdata[item])
        }
    }
}
// 결국 출력되는 건 이거

function understandingCallback(datas, callback){
    a.push(datas); // [{name : "park", id:"pmj"}]
    callback(datas); // print123({name : "park", id:"pmj"})
}

understandingCallback({name : "park", id:"pmj"}, print123)

// 1. 콜백 함수에 대한 정의가 '함수'로 들어가면
// ㄴ 이 호출되는 함수 '안에' 콜백함수를 호출하는게 있을 것 callback() 이런 식으로
// ㄴ callback 자리에다가 print123() 만 들어가게 되는 것이니


// 지금 이 함수는 print123 결과가 출력되는 것과도 같게 되는 것
