const express = require('express');
const Person = require('./layer2');
const Cat = require('./layer3');

const abcd = [10, 20, 30];

try{
    console.log(abcd[0]);
    const a = new Person('kim');
    console.log(a.name);
    //console.log(a.introduce());
    //sjkfhgjkdgkd;
} catch(e){
    console.log("에러는 최상단 레이어 catch에서 잡아줍니다.");
    console.log(e.message);
    // if (e.message == "layer2 error"){
    //     console.log("layer2 error 받기");
    // }
}

// 여기서 보고자 하는 것 : 에러가 발생하면 그 아래 코드는 실행되지 않고 catch로 간다는 것

// console.log(a.name) 출력이 되지 않는 이유는, name =park 로 넘겨서 이게 뒷단에서 오류 조건이 되기 때문
// 만약 name = kim으로 넘겼으면 정상 출력됨