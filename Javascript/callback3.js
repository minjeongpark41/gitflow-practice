// function getData(callback){
//     let data = "";
//     setTimeout(() => {
//         data = "my-data";
//         return callback(data);
//     }, 1000);
// }
//
// getData(function (result){
//     console.log(result); // my -data
// })

//

function getData(){
    let data = "";
    return new Promise((resolve) => {
        setTimeout(() => {
            data = "my - data22";
            return data;
        },0);
    })
}

getData()
    .then(() => {
        console.log("안녕")
        return new Promise((resolve) => {
            setTimeout(() => {
                let a = "aaaaa";
                resolve(a)
            },0);
        })
    })
    .then((b) => {
        console.log(b);
})

// 내가 예상하는건 -> 안녕, aaaaa 가 나오는건데...

// function getData(){
//     let data = "";
//     return new Promise ((resolve) => {
//         setTimeout(() => {
//             data = "my-data333";
//             resolve(data);
//         }, 1000);
//     })
// }
//
// async function getResult(){
//     const result = await getData();
//     console.log(result); // my-data333
// }