const Cat = require('./layer3');

function Person(name){
    this.name = name;
    this.introduce = function(){
        return 'my name is ' + this.name;
    }
    if (this.name == 'park'){
        throw new Error("layer2 error");
    }
    const b = new Cat('egg');
    console.log(b.name);
}

module.exports = Person;


// 여기서 보고자 하는 것 : 에러가 발생하면 최상단 layer의 catch로 가서 에러 처리를 해준다는 점
// 최상단 클래스를 제외한 나머지 클래스에서의 예외처리는 반드시 Throw를 해주어야, '처리'가 가능

// 지금 문제가 되고 있는 상황 : error 처리 부분을 exports 해주지 못하고 있다는 점