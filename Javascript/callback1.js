const testError = require('../Model/testError');
const callbakc2 = require('./callback2');

const a = 'park';
try{
    if(a == 'park'){
        throw new testError("callback1 에러 메시지", 404);
        //console.log("오류오류");
        // ㄴ 이럴 경우 catch 부분에서, console.log(e)는 그대로 "오류오류" 출력
    }
    callback2.chocolate(aa, bb);
} catch (e) {
    console.log(e);
}

// testError {
//     name: 'testError',
//     message: 'callback1 에러 메시지',
//     status: 404
// }
