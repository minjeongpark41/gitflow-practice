let arr = ['a', 'b'];
let [first, second] = arr;
console.log(first, second);
// 결과 -> a, b

let [a, , ...b] = [1,2,3,4,5,6];
console.log(a,b)
// 결과 -> 1 [ 3, 4, 5, 6 ]

// 할당 - 분해 위치 (할당 길이가 길더라도 에러나지 않음. undefined 취급)


// was 파일을 보면
// ㄴ {req, tnUserId} = {req}
//                       ㄴ req:req인데 key, value 같아서 생략된 것