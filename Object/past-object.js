var person1 = {
    'name' : 'park',
    'introduce' : function(){
        return 'My name is ' + this.name;
    }
}

var person2 = {
    'name' : 'kim',
    'introduce' : function(){
        return 'My name is ' + this.name;
    }
}

console.log(person2.introduce());