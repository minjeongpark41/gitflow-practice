function Person(){} // 함수 하나 호출

var p = new Person(); // new 생성자 통해 객체 만듦, var p = {} 와 동일
p.name = 'park';
p.intoduce = function(){
    return 'My name is ' + this.name;
}

console.log(p.intoduce());